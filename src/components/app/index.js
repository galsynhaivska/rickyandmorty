
import { useState } from "react";
import MainPage from "../../pages/main-page";
import CardScreen from "../../pages/card-screen";
import Details from "../../pages/details";
import { BrowserRouter, Routes } from "react-router-dom";
import { Route } from "react-router";
import { req } from "../../methods/methods";


export const pathBasis = "https://rickandmortyapi.com/api/";
export let category ="", id = "";
export let url = pathBasis;

export default function App () {
    const [screen2, setScreen2] = useState({status: false, display: null});
  const [reqData, setReqData] = useState([]);
  
  function clickMenu(ev, d){
    console.log(url, screen2.display, category)
   category = d.substr(0, d.length-1).toLowerCase()
    setScreen2({ status: true, display: d}) 
    url = `${pathBasis}${category}`
    req(url).then((info) => {  
      setReqData(info);
      
    });
  }
  console.log(url, screen2.display, category)

    return (
      <>
      {
       <BrowserRouter>
          <Routes>
            <Route path="/" element={<MainPage click={clickMenu}></MainPage>}></Route>
            <Route path={`/${category}`}  element={ screen2.status ? <CardScreen displayName={screen2.display}  category={category} data={reqData} click={clickMenu}></CardScreen> : null} ></Route>
            <Route path={`/${category}/:id`} element={ <Details  category={category} url={`${url}/${id}`} click={clickMenu}></Details> }></Route>
          </Routes>
        </BrowserRouter>
      }
       
      </>
      
    )
}

