import Card from "./card/";

import "./cards.css";


export default function Cards ({className, className1, data, category, displayName}){
    console.log(data.results)

    return(    
        <div className="cards">  
        {  
                data.results ? data.results.map((e, i) => {   
                    return <Card key={i + Math.random()} className={"card-item "  + category} className1="" category={category} displayName={displayName} data={e}></Card>    
                }) : data.results
        }  
            
        </div>

       
    )
}


