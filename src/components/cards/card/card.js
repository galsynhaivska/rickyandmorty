import Image from "../../image/";
import {Link} from "react-router-dom";

import "./card.css";

export default function Card ({className, className1, data, category, displayName}) {
    console.log(category)
    const newObj = createNewObj(data, category)
    return (
        <>
        <div className={className} >
            <Link className="link-card" to={`/${category}/${newObj.id}`}>
            <div className={"card-img "  + category}> 
                <Image  src={newObj.img} alt={newObj.name}></Image>
            </div>
            <p className={"info-name " + className1}>{newObj.title}</p>
            <p className={"info-date " + category}>{newObj.date}</p>
            <p className="species-name">{newObj.species}</p>
            </Link>
        </div>
       
        </>
    )
}

function createNewObj (obj, group) {
   
       if(group.includes("character")) {
            return {
                id: obj.id,
                title: obj.name,
                species: obj.species,
                img : obj.image,   
                url: obj.url            
            }
        }else if(group.includes("location")){
            return {
                id: obj.id,
                title: obj.name,
                species: obj.type,
                date: obj.created,    
                url: obj.url             
            }
        }else if(group.includes("episode")){
            return {
                id: obj.id,
                title: obj.name,
                species: obj.episode,
                date:  obj.air_date,      
                url: obj.url           
            }
        }      
    
}
