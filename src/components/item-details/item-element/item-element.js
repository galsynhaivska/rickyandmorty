
export default function ItemElement({property, title}){
    return (
        <div className="details-item">
        <p className="l-item">{title}</p>
        <p className="p-item"> {property}</p>
    </div>
    )
}