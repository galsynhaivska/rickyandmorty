import "../../../pages/details/details.css";
import "./item-url.css";

export default function ItemUrl({title, property, category, itemurl, clickbtn}){
   
    console.log(category)
    return (
        
            <div className="details-item" onClick={(ev) => {
                clickbtn(ev, property, itemurl)}}>
                <p className="l-item">{title}</p>
                {
                itemurl === '' ?  <p className="p-item">{property}</p> : 
                <p className="p-item btn-place">{property}...</p>
            }
            </div>
       
    )
}