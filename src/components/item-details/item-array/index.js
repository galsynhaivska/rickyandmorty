

import "../../../pages/details/details.css";

export default function ItemArray({title, property}){
    console.log(title, title.toLowerCase(), property)
    return (
        
        <div className="details-item">
            { Array.isArray(property)  ? 
            <div className="p-item btn-episode" data={property} >{title} </div>  :
            <p className="p-item">No episode</p>}
        </div>   
        
    )
}