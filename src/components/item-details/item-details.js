import Image from "../image";
import ItemElement from "./item-element";
import ItemUrl from "./item-url/";
import ItemArray from "./item-array";
import {req } from "../../methods/methods";
import { useState } from "react" ;
import Card from "../cards/card/";

import {pathBasis} from "../app/";
import "./../../pages/details/details.css";
import "../cards/card/card.css";
import "./item-details.css";


export default function ItemDetails ({category, information, url, click,  clickbtn}){
/*    console.log(Object.keys(information))
    console.log(Object.values(information))*/
    console.log(url)
    const [showProp, setShowProp] = useState({status: false, display: null, categoryProp: null});
    const [propData, setPropData] = useState({status: false, display: null});

    const [screen2, setScreen2] = useState({})
    const [epiData, setEpiData] = useState({})
    
    function clickBtn(ev, d, urlProp){   
        setShowProp({ status: true, display: d, categoryProp: urlProp.slice(pathBasis.length, urlProp.lastIndexOf("/") )}) 
        req(urlProp).then((info) => {  
          setPropData(info);
          console.log(urlProp)
          console.log(info)
          console.log(urlProp.slice(pathBasis.length, urlProp.lastIndexOf("/") ))
        });
    }
   
    
   
    return(
        
            <div className="container-details">
                <div className="details">
                    <div className="det-image">
                        <Image src={information.image} alt={information.name}></Image> 
                    </div>
                    <div className="det-info">
                        <ItemElement title="Species: " property={information.species}></ItemElement>
                        <ItemElement title="Gender: " property={information.gender}></ItemElement>
                        <ItemElement title="Status: " property={information.status}></ItemElement>
                        <ItemUrl title="Origin: " category={category} property={information.originName} itemurl={information.originUrl} clickbtn={clickBtn}></ItemUrl>
                        <ItemUrl title="Location: " category={category} property={information.locationName} itemurl={information.locationUrl} clickbtn={clickBtn}></ItemUrl>
                        <ItemArray title="Episodes" property={information.episode}></ItemArray>
                        {
                            information.type === "" ? null :  
                                <ItemElement title="Type: " property={information.type}></ItemElement>
                        }
                       <ItemElement title="Created: " property={information.created}></ItemElement>
                        {
                           showProp.status ?                          
                                <div className="show-property details-card"> 
                                    <div className="close-btn">✕</div>
                                    <Card className="card-item " className1="details-position"  key="" category={showProp.categoryProp} displayName={showProp.display} data={propData}> </Card>   
                                </div>
                           : null
                        }
                    </div>
                </div> 
            </div>
    )
}