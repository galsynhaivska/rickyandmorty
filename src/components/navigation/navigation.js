import Image from "../image";
import logo1 from "../../image/logo1.png";
import NavItem from "./nav-item";
import { Link } from "react-router-dom";

import "./navigation.css";

export default function Navigation ({category, click}) {
    return (
        <div className="header"> 
            <div className="div-logo-img">
                <Link to={'/'}>
                    <Image className="logo-img" src={logo1} alt="logo"></Image>
                </Link>
            </div>
            <nav className="nav-container">
                <NavItem className="nav-item" title="Characters" category={category} click={click}></NavItem>
                <NavItem className="nav-item" title="Locations" category={category} click={click}></NavItem>
                <NavItem className="nav-item" title="Episodes" category={category} click={click}></NavItem>
            </nav>
        </div>
    )
}