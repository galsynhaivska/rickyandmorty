import { Link } from "react-router-dom"; 

export default function NavItem ({className, category, title, click}) {
    return (
        <Link className="link-navigation" to={`/${title.slice(0, title.length -1).toLowerCase()}`}>
            <div className={className} category={category} onClick={(ev) => {
                click(ev, title)
            }}>{title}</div>
        </Link>
    )
}