import "./footer.css";

export default function Footer ({className}) {
    return (
        <footer className={className}>
            <div> Make with ❤️ for the MobProgramming team </div>
        </footer>
    )
   
}