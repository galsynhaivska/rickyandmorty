import glass from "../../image/glass.png";

import "./input-search.css";

export default function InputSearch ({className, placeholder}) {
    return (
      
            <div className={className}>
                <div className="search-item">
                    <img className="input-image" src={glass} alt="search"></img>
                    <input className="input-field" placeholder={placeholder}></input>
                </div>
            </div>

    )
}