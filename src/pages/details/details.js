import { useParams } from "react-router";
import { useState, useEffect } from "react";
import { req, showItem } from "../../methods/methods";
import Navigation from "../../components/navigation";
import Image from "../../components/image";
import arrowBack from "../../image/arrow_back.png";
import ItemDetails from "../../components/item-details/item-details";
import Footer from "../../components/footer/footer";
import { Link } from "react-router-dom";

import "./details.css";



export default function Details({category, click, url}) {
    
    const {id} = useParams()   
    const urlItem = `${url}${id}`
    console.log(urlItem)
    console.log(`${url}${id}`)

    const [information, setInformation] = useState({})

    useEffect(() => {
        req(`${url}${id}`).then((info) => {
            console.log(info)
            setInformation(showItem(info, category)) 
        })
    }, [])  
    console.log(information)  
   
    return (          
        <>
            <div className="top-container">
                <Navigation category={category} click={click}></Navigation>             
                <div className="screen-title">
                    <Link to={ `/${category}`}>
                        <div className="go-back">
                            <Image className="arrow" src={arrowBack} alt="arrow"></Image>
                            <p className="text-back">Go Back</p>
                        </div>                   
                    </Link>
                    <h1 className="title-page">{information.name}</h1>
                </div>
            </div> 
            <ItemDetails category={category} information={information} url={urlItem} click={click}></ItemDetails>
            <Footer className="footer"></Footer>
        </>
    )
}