import Navigation from "../../components/navigation";
import Footer from "../../components/footer";
import Image from "../../components/image";
import arrowBack from "../../image/arrow_back.png";
import { Link } from "react-router-dom";
import Cards from "../../components/cards";

import "../main-page/main-page.css";
import "../card-screen/card-screen.css";

export default function CardScreen({displayName, category, click, data}){
    return (
        <> 
            <div className="container">
                <div className="top-container">
                    <Navigation category={category} click={click}></Navigation>             
                    <div className="screen-title">
                        <Link to={'/'}>
                            <div className="go-back">
                                <Image className="arrow" src={arrowBack} alt="arrow"></Image>
                                <p className="text-back">Go Back</p>
                            </div>                   
                        </Link>
                        <h1 className="title-page">{displayName}</h1>
                    </div>
                </div>
                <Cards data={data} category={category} displayName={displayName}></Cards>
                <Footer className="footer"></Footer>
            </div>
        </>    
    )
}