import Navigation from "../../components/navigation";
import Image from "../../components/image";
import InputSearch from "../../components/input-search";
import Footer from "../../components/footer";
import rick from "../../image/rickmorty.png";

import "./main-page.css";

export default function MainPage({click}){
    return (
        <>
            <div className="container">
                <div className="top-container">
                    <Navigation click={click}></Navigation>
                    <div className="div-img-rick">
                        <Image className="img-rick" src={rick} alt="Rick and Morty"></Image>
                    </div>
                    <InputSearch className="search-container" placeholder="Filter by name or episode (ex. S01 or S01E02)"></InputSearch>        
                    </div>   
                    <Footer className="footer"></Footer>
            </div>
        </>
        
    )
}