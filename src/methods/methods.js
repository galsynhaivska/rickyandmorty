
export async function req (url) {
    const rez = await fetch(url);
    return await rez.json();
}

export function showName (data=[]) {
    return data.results.map(({name}) => {
        return <div>{name}</div>
    })
}

export function showItem (info={}, group) { 
    if(group === "character"){
           return {
            name: info.name,
            image: info.image,
            species: info.species,
            gender: info.gender,
            status: info.status,
            type: info.type,
            episode: info.episode,
            locationName: info.location.name.slice(0, 12),
            locationUrl: info.location.url,
            originName: info.origin.name.slice(0, 12),
            originUrl: info.origin.url,
            created: `${info.created.slice(0, info.created.indexOf("T"))}`
        }
    } else if(group === "location"){
        return {
         name: info.name,
         type: info.type,
         dimension: info.dimension,
         characters: info.residents,
         created: `${info.created.slice(0, info.created.indexOf("T"))}`
     }
 
    }else if(group === "episode"){
        return {
         name: info.name,
         airdate: info.air_date,
         characters: info.characters,
         episode: info.episode,         
         created: `${info.created.slice(0, info.created.indexOf("T"))}`
     }
    }
}